import firebase from 'firebase';

const config = {
  apiKey: '-VFAIzaSyAtZ05-8pV5oDaDnXz90bS6yL-9rMTo',
  authDomain: 'mobile-task-c436c.firebaseapp.com',
  databaseURL: 'https://mobile-task-c436c.firebaseio.com',
  projectId: 'mobile-task-c436c',
  storageBucket: 'mobile-task-c436c.appspot.com',
  messagingSenderId: '296625635237',
};

const app = firebase.initializeApp(config);

export const db = app.database();