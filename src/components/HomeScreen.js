import React, { Component } from 'react';
import { Container, Footer, Body, Text, Card, CardItem, Spinner} from 'native-base';
import { StyleSheet, View, ScrollView} from 'react-native';


export default class HomeScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isLoading: true,
        dataSource: null,
      };
    }
  
    static navigationOptions = {
      title: 'Mobile task',
    };
  
    componentDidMount() {
      return fetch('https://www.npoint.io/documents/a21afc7a71ef2b3d4e6a')
        .then((response) => response.json())
        .then((responseJson) => {
  
          this.setState({
            isLoading: false,
            dataSource: responseJson.contents,
          })
  
        })
  
        .catch((error) => {
          console.log(error);
        });
    }
  
    render() {
  
      if (this.state.isLoading) {
        return (
          <View style={styles.spinner}>
            <Spinner />
          </View>
        )
      }
      else {
  
        let movies = this.state.dataSource.map((val, key) => {
          return <View key={key}>
            <Card>
              <CardItem header button onPress={() => this.props.navigation.navigate('Details', { movie: val.title })}>
                <Body>
                  <Text>{val.title}</Text>
                </Body>
              </CardItem>
            </Card>
          </View>
        });
  
        return (
          <Container>
            <ScrollView>
              <View style={styles.container}>
                {movies}
              </View>
              <Footer />
            </ScrollView>
          </Container>
        );
      }
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    spinner: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    }
  });
