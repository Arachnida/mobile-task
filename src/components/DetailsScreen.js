import React, { Component } from 'react';
import { Text, Item, Input } from 'native-base';
import { StyleSheet, View, TouchableHighlight, Alert } from 'react-native';
import { addItem } from '../services/CommentService.js';

export default class DetailsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            name: '',
            error: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        this.setState({
            name: e.nativeEvent.text
        });
    }
    handleSubmit() {
        addItem(this.state.name);
        Alert.alert(
            'Comentario enviado exitosamente'
        );
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('movie', 'Pelicula seleccionada'),
        };
    };
    
    render() {
        const { navigation } = this.props;
        const val = navigation.getParam('val', 'Pelicula seleccionada');

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Item rounded style={styles.detailsinput}>
                    <Input
                        placeholder='Write Comment'
                        onChange={this.handleChange}
                    />
                    <TouchableHighlight
                        underlayColor="white"
                        onPress={this.handleSubmit}
                    >
                        <Text>
                            Add
                </Text>
                    </TouchableHighlight>
                </Item>
                <Text style={{ padding: 10, fontSize: 42 }}>
                    {this.state.text}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF'
    },
    detailsinput: {
        position: 'absolute',
        marginLeft: 1,
        marginRight: 1,
        paddingRight: 10,
        bottom: 2
    }
});