import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DetailsScreen } from '../components/DetailsScreen.js';
import { db } from '../config/db';

let itemsRef = db.ref('/items');

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#B6A6BB',
    }
  })

export default class ListComment extends Component {

    state = {
        items: []
    }

    componentDidMount() {
        itemsRef.on('value', (snapshot) => {
            let data = snapshot.val();
            let items = Object.values(data);
            this.setState({items});
         });
    }
    
    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.items.length > 0
                    ? <DetailsScreen items={this.state.items} />
                    : <Text>No items</Text>
                }
            </View>
        )
    }
}