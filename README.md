# mobile-task Jorge Soto 

## How to run the code

1. Clone the repository
2. npm install
3. react-native run-android

## Explanation of architecture

Project plan: https://docs.google.com/document/d/1H6Z5mrMEyw-HarThnSMy3efm6H-rV_Rgfm72kT0dpi8/edit?usp=sharing

La aplicacion fue creada usando React Native y Firebase.

Dentro del folder src se encuentran:
* components: contiene los componentes a utilizar en la aplicacion.
* config: contiene la configuracion de la base de datos en FireBase.
* services: contiene dos archivos que manejan tanto el create y el get

Desde el "App.js" se manejan las vistas de la aplicacion mediante React Navigator.

## If you had more time, what would you like to improve?
* Hubiera usado "FlatList" en vez de ScrollView para aumentar el performance debido a que la lista de las peliculas es bastante grande.

* Hubiera separado los componentes aun mas.

* Definitivamente hubiera hecho mas commits cada vez que terminara un task.
