import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './src/components/HomeScreen.js';
import DetailsScreen from './src/components/DetailsScreen.js';
import ListComment from './src/services/ListComment.js';

type Props = {};

export default class App extends Component<Props> {

  render() {
    return <RootStack />;
  }
}

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    ListItemScreen: { screen: ListComment},
  },
  {
    initialRouteName: 'Home',
  }
);
